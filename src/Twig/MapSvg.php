<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 09/02/2019
 * Time: 17:00
 */

namespace App\Twig;

use App\Entity\DataProvince;
use App\Entity\District;
use App\Entity\Province;
use App\Entity\Region;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\JsonManifestVersionStrategy;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MapSvg implements RuntimeExtensionInterface
{

    private $translator;
    private $router;
    private $json_file = __DIR__ . '/../../public/build/manifest.json';
    private $managerRepository;

    public function __construct(TranslatorInterface $translator, UrlGeneratorInterface $router, ManagerRegistry $managerRegistry)
    {
        $this->translator = $translator;
        $this->router = $router;
        $this->managerRepository = $managerRegistry;
    }

    public function showMap($type, $entities)
    {
        $data = [];

        switch ($type) {
            case 'province':
                $data = $this->getMapDataProvince($entities);
                break;
            case 'region':
                $data = $this->getMapDataRegion($entities);
                break;
            case 'district':
                $data = $this->getMapDataDistrict($entities);
                break;
        }

        $arrJson = $data[0] ?? [];
        $svg = $data[1] ?? '';

        // Data by provinces
        $listProvince = $this->getDataProvinces();

        $params = [
            'arrJson' => $arrJson,
            'arrJsonEncode' => rawurlencode(json_encode($arrJson)),
            'svg' => $svg,
        ];
        $str = '<!-- ' . json_encode($arrJson) . '-->' . PHP_EOL;
        $str .= <<<SVG
<svg
    data-json="{$params['arrJsonEncode']}"
    data-province="{$listProvince}"
    data-svg="{$params['svg']}"
    class="svg-map"
    style="margin:0 auto; width:300px; height:563px;"></svg>
SVG;

        return $str;
    }

    private function getDataProvinces()
    {
        // Show "Cas confirmés"
        $dataProvinceEntities = $this->managerRepository->getRepository(DataProvince::class)->findBy(['typeCase' => 1]);

        $listProvince = [];
        foreach ($dataProvinceEntities as $dataProvince) {

            $provinceId = $dataProvince->getProvince()->getId();
            $dataProvinceNumber = $dataProvince->getNumber();



            $listProvince['province_' . $provinceId] = ['data' => $dataProvinceNumber, 'display' => $dataProvinceNumber];
        }



        $data = json_encode($listProvince);
        $data = rawurlencode($data);


        return $data;

    }

    private function getData(array $arrArg)
    {
        $arrJson = [];
        $entities = $arrArg['entities'] ?? null;
        $svg = $arrArg['svg'] ?? '';
        $tooltipTrans = $arrArg['tooltipTrans'] ?? '';
        $routeName = $arrArg['routeName'] ?? '';
        $routeItemName = $arrArg['routeItemName'] ?? '';


        $candidatName = '';

        foreach ($entities as $item) {
            $itemLibelle = $item->getName() ;
            $itemId = $item->getId() ;

            $tooltip = $this->translator->trans(
                $tooltipTrans,
                ['%libelle%' => $itemLibelle, '%candidat%' => $candidatName ]);


            array_push($arrJson, [
                'selector' => '#id' . $itemId,
                'libelle' => $itemLibelle,
//                'tooltip' => $tooltip,
//                'url' => '',
                'active' => 'false'
            ]);
        }

        return [$arrJson, $svg];
    }

    private function getMapDataDistrict($entities=null)
    {
        // All District
        if (is_null($entities)) {
            $entities = $this->managerRepository->getRepository(District::class)->findAll();
        }

        $svg = 'build/images/maps/districts_nocolor.svg';

        // Json exists ?
        $fs = new Filesystem();
        if ($fs->exists($this->json_file)) {
            $version = new JsonManifestVersionStrategy($this->json_file);
            $package = new Package($version);
            $svg = $package->getUrl($svg);
        }

        $tooltipTrans = 'tooltip_resultat_district';
        $routeName = 'resultat_one_district_candidat';
        $routeItemName = 'district';

        $arrArg = [
            'entities' => $entities,
            'svg' => $svg,
            'tooltipTrans' => $tooltipTrans,
            'routeName' => $routeName,
            'routeItemName' => $routeItemName
        ];
        $arr = $this->getData($arrArg);
        return $arr;
    }

    private function getMapDataRegion($entities=null)
    {
        // All entities
        if (is_null($entities)) {
            $entities = $this->managerRepository->getRepository(Region::class)->findAll();
        }

        $svg = 'build/images/maps/regions_nocolor.svg';

        // Json exists ?
        $fs = new Filesystem();
        if ($fs->exists($this->json_file)) {
            $version = new JsonManifestVersionStrategy($this->json_file);
            $package = new Package($version);
            $svg = $package->getUrl($svg);
        }

        $tooltipTrans = 'tooltip_resultat_district';
        $routeName = 'resultat_one_district_candidat';
        $routeItemName = 'district';

        $arrArg = [
            'entities' => $entities,
            'svg' => $svg,
            'tooltipTrans' => $tooltipTrans,
            'routeName' => $routeName,
            'routeItemName' => $routeItemName
        ];
        $arr = $this->getData($arrArg);
        return $arr;
    }

    private function getMapDataProvince($entities=null)
    {
        // All entities
        if (is_null($entities)) {
            $entities = $this->managerRepository->getRepository(Province::class)->findAll();
        }

        $svg = 'build/images/maps/provinces_nocolor.svg';

        // Json exists ?
        $fs = new Filesystem();
        if ($fs->exists($this->json_file)) {
            $version = new JsonManifestVersionStrategy($this->json_file);
            $package = new Package($version);
            $svg = $package->getUrl($svg);
        }

        $tooltipTrans = 'tooltip_resultat_district';
        $routeName = 'resultat_one_district_candidat';
        $routeItemName = 'district';

        $arrArg = [
            'entities' => $entities,
            'svg' => $svg,
            'tooltipTrans' => $tooltipTrans,
            'routeName' => $routeName,
            'routeItemName' => $routeItemName
        ];
        $arr = $this->getData($arrArg);
        return $arr;
    }
}
