<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\ApiPlatform\DuplicateCommune;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/communes/{id}/duplicate",
 *              "controller"=DuplicateCommune::class,
 *          }
 *     },
 *     collectionOperations=
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *      }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "name"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "name": "ipartial",
 *          "province": "exact",
 *          "region": "exact",
 *          "district": "exact"
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CommuneRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Commune
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     *
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $disabledAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="communes")
     * @Groups({"group:read"})
     */
    private $province;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="communes")
     * @Groups({"group:read"})
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\District", inversedBy="communes")
     * @Groups({"group:read"})
     */
    private $district;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quartier", mappedBy="commune")
     */
    private $quartiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataCommune", mappedBy="commune")
     */
    private $dataCommunes;

    public function __construct()
    {
        $this->quartiers = new ArrayCollection();
        $this->dataCommunes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): self
    {
        $this->province = $province;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }

    public function setDistrict(?District $district): self
    {
        $this->district = $district;

        return $this;
    }

    /**
     * @return Collection|Quartier[]
     */
    public function getQuartiers(): Collection
    {
        return $this->quartiers;
    }

    public function addQuartier(Quartier $quartier): self
    {
        if (!$this->quartiers->contains($quartier)) {
            $this->quartiers[] = $quartier;
            $quartier->setCommune($this);
        }

        return $this;
    }

    public function removeQuartier(Quartier $quartier): self
    {
        if ($this->quartiers->contains($quartier)) {
            $this->quartiers->removeElement($quartier);
            // set the owning side to null (unless already changed)
            if ($quartier->getCommune() === $this) {
                $quartier->setCommune(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataCommune[]
     */
    public function getDataCommunes(): Collection
    {
        return $this->dataCommunes;
    }

    public function addDataCommune(DataCommune $dataCommune): self
    {
        if (!$this->dataCommunes->contains($dataCommune)) {
            $this->dataCommunes[] = $dataCommune;
            $dataCommune->setCommune($this);
        }

        return $this;
    }

    public function removeDataCommune(DataCommune $dataCommune): self
    {
        if ($this->dataCommunes->contains($dataCommune)) {
            $this->dataCommunes->removeElement($dataCommune);
            // set the owning side to null (unless already changed)
            if ($dataCommune->getCommune() === $this) {
                $dataCommune->setCommune(null);
            }
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setRegionValue()
    {
        $this->region = $this->getDistrict()->getRegion();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setProvinceValue()
    {
        $this->province = $this->getDistrict()->getRegion()->getProvince();
    }
}
