<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\ApiPlatform\DuplicateRegion;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/regions/{id}/duplicate",
 *              "controller"=DuplicateRegion::class,
 *          }
 *     },
 *     collectionOperations=
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *      }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "name"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "name": "ipartial",
 *          "province": "exact",
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\RegionRepository")
 */
class Region
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $disabledAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="regions")
     * @Groups({"group:read"})
     */
    private $province;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\District", mappedBy="region")
     */
    private $districts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commune", mappedBy="region")
     */
    private $communes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quartier", mappedBy="region")
     */
    private $quartiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataRegion", mappedBy="region")
     */
    private $dataRegions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateRegion", mappedBy="region")
     */
    private $dataDateRegions;

    public function __construct()
    {
        $this->districts = new ArrayCollection();
        $this->communes = new ArrayCollection();
        $this->quartiers = new ArrayCollection();
        $this->dataRegions = new ArrayCollection();
        $this->dataDateRegions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): self
    {
        $this->province = $province;

        return $this;
    }

    /**
     * @return Collection|District[]
     */
    public function getDistricts(): Collection
    {
        return $this->districts;
    }

    public function addDistrict(District $district): self
    {
        if (!$this->districts->contains($district)) {
            $this->districts[] = $district;
            $district->setRegion($this);
        }

        return $this;
    }

    public function removeDistrict(District $district): self
    {
        if ($this->districts->contains($district)) {
            $this->districts->removeElement($district);
            // set the owning side to null (unless already changed)
            if ($district->getRegion() === $this) {
                $district->setRegion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commune[]
     */
    public function getCommunes(): Collection
    {
        return $this->communes;
    }

    public function addCommune(Commune $commune): self
    {
        if (!$this->communes->contains($commune)) {
            $this->communes[] = $commune;
            $commune->setRegion($this);
        }

        return $this;
    }

    public function removeCommune(Commune $commune): self
    {
        if ($this->communes->contains($commune)) {
            $this->communes->removeElement($commune);
            // set the owning side to null (unless already changed)
            if ($commune->getRegion() === $this) {
                $commune->setRegion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quartier[]
     */
    public function getQuartiers(): Collection
    {
        return $this->quartiers;
    }

    public function addQuartier(Quartier $quartier): self
    {
        if (!$this->quartiers->contains($quartier)) {
            $this->quartiers[] = $quartier;
            $quartier->setRegion($this);
        }

        return $this;
    }

    public function removeQuartier(Quartier $quartier): self
    {
        if ($this->quartiers->contains($quartier)) {
            $this->quartiers->removeElement($quartier);
            // set the owning side to null (unless already changed)
            if ($quartier->getRegion() === $this) {
                $quartier->setRegion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataRegion[]
     */
    public function getDataRegions(): Collection
    {
        return $this->dataRegions;
    }

    public function addDataRegion(DataRegion $dataRegion): self
    {
        if (!$this->dataRegions->contains($dataRegion)) {
            $this->dataRegions[] = $dataRegion;
            $dataRegion->setRegion($this);
        }

        return $this;
    }

    public function removeDataRegion(DataRegion $dataRegion): self
    {
        if ($this->dataRegions->contains($dataRegion)) {
            $this->dataRegions->removeElement($dataRegion);
            // set the owning side to null (unless already changed)
            if ($dataRegion->getRegion() === $this) {
                $dataRegion->setRegion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateRegion[]
     */
    public function getDataDateRegions(): Collection
    {
        return $this->dataDateRegions;
    }

    public function addDataDateRegion(DataDateRegion $dataDateRegion): self
    {
        if (!$this->dataDateRegions->contains($dataDateRegion)) {
            $this->dataDateRegions[] = $dataDateRegion;
            $dataDateRegion->setRegion($this);
        }

        return $this;
    }

    public function removeDataDateRegion(DataDateRegion $dataDateRegion): self
    {
        if ($this->dataDateRegions->contains($dataDateRegion)) {
            $this->dataDateRegions->removeElement($dataDateRegion);
            // set the owning side to null (unless already changed)
            if ($dataDateRegion->getRegion() === $this) {
                $dataDateRegion->setRegion(null);
            }
        }

        return $this;
    }
}
