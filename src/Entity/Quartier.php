<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\ApiPlatform\DuplicateQuartier;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/quartiers/{id}/duplicate",
 *              "controller"=DuplicateQuartier::class,
 *          }
 *     },
 *     collectionOperations=
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *      }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "name"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "name": "ipartial",
 *          "province": "exact",
 *          "region": "exact",
 *          "district": "exact",
 *          "commune": "exact"
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\QuartierRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Quartier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $disabledAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="quartiers")
     * @Groups({"group:read"})
     */
    private $province;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="quartiers")
     * @Groups({"group:read"})
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\District", inversedBy="quartiers")
     * @Groups({"group:read"})
     */
    private $district;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Commune", inversedBy="quartiers")
     * @Groups({"group:read"})
     */
    private $commune;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): self
    {
        $this->province = $province;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }

    public function setDistrict(?District $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getCommune(): ?Commune
    {
        return $this->commune;
    }

    public function setCommune(?Commune $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setDistrictValue()
    {
        $this->district = $this->getCommune()->getDistrict();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setRegionValue()
    {
        $this->region = $this->getCommune()->getDistrict()->getRegion();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate()
     */
    public function setProvinceValue()
    {
        $this->province = $this->getCommune()->getDistrict()->getRegion()->getProvince();
    }
}
