<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Loader\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\ApiPlatform\DuplicateTranslation;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"group:read"}},
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/translations/{id}/duplicate",
 *              "controller"=DuplicateTranslation::class,
 *          }
 *     },
 *     collectionOperations=
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           }
 *      }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "translation"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "translation": "ipartial",
 *          "translationType": "exact",
 *          "locale": "exact",
 *          "keytranslate": "ipartial",
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TranslationRepository")
 */
class Translation implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"group:read"})
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group:read"})
     */
    private $keytranslate;

    /**
     * @ORM\Column(type="text")
     * @Groups({"group:read"})
     */
    private $translation;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sex", mappedBy="TranslationDb")
     */
    private $sexes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TypeCase", mappedBy="TranslationDb")
     */
    private $typecases;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TranslationType", inversedBy="translations",cascade={"persist"})
     * @Groups({"group:read"})
     */
    private $translationType;

    public function __construct()
    {
        $this->sexes = new ArrayCollection();
        $this->typecases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getKeytranslate(): ?string
    {
        return $this->keytranslate;
    }

    public function setKeytranslate(string $keytranslate): self
    {
        $this->keytranslate = $keytranslate;

        return $this;
    }

    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    public function setTranslation(string $translation): self
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Load data to entity.
     * For example: imagine that entity has `domain`, `locale`, `key` and `translation` params
     * This method may be called as
     * ```
     * $entity->load([
     *    'domain' => $domain,
     *    'locale' => $locale,
     *    'key' => $key,
     *    'translation' => $translation,
     * ]);
     * ```
     * and return valid entity for store in database.
     *
     * @param array $params
     *
     * @return EntityInterface
     */
    public function load(array $params): EntityInterface
    {
        // TODO: Implement load() method.
    }

    /**
     * @return Collection|Sex[]
     */
    public function getSexes(): Collection
    {
        return $this->sexes;
    }

    public function addSex(Sex $sex): self
    {
        if (!$this->sexes->contains($sex)) {
            $this->sexes[] = $sex;
            $sex->addTranslationDb($this);
        }

        return $this;
    }

    public function removeSex(Sex $sex): self
    {
        if ($this->sexes->contains($sex)) {
            $this->sexes->removeElement($sex);
            $sex->removeTranslationDb($this);
        }

        return $this;
    }

    public function getTranslationType(): ?TranslationType
    {
        return $this->translationType;
    }

    public function setTranslationType(?TranslationType $translationType): self
    {
        $this->translationType = $translationType;

        return $this;
    }

    /**
     * @return Collection|TypeCase[]
     */
    public function getTypeCases(): Collection
    {
        return $this->typecases;
    }

    public function addTypeCase(TypeCase $typeCase): self
    {
        if (!$this->typecases->contains($typeCase)) {
            $this->typecases[] = $typeCase;
            $typeCase->addTranslationDb($this);
        }

        return $this;
    }

    public function removeTypeCase(TypeCase $typeCase): self
    {
        if ($this->typecases->contains($typeCase)) {
            $this->typecases->removeElement($typeCase);
            $typeCase->removeTranslationDb($this);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->getTranslation();
    }

    public function setName($name): ?self
    {
        return $this->setTranslation($name);
    }
}
