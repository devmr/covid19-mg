<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\Table;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *     },
 *     collectionOperations =
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TypeCaseRepository")
 * @Table(indexes={@Index(name="pos_idx", columns={"position"})})
 */
class TypeCase implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     *
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateType", mappedBy="typeCase")
     */
    private $dataAlls;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataRegion", mappedBy="typeCase")
     */
    private $dataRegions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataProvince", mappedBy="typeCase")
     */
    private $dataProvinces;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDistrict", mappedBy="typeCase")
     */
    private $dataDistricts;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataType", mappedBy="typeCase")
     */
    private $dataTypes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateProvince", mappedBy="typeCase")
     */
    private $dataDateProvinces;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateRegion", mappedBy="typeCase")
     */
    private $dataDateRegions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataDateDistrict", mappedBy="typeCase")
     */
    private $dataDateDistricts;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"group:read"})
     */
    private $settings;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Translation", inversedBy="typecases")
     * @JoinTable(name="type_case_translation_db")
     * @Groups({"group:read"})
     */
    private $TranslationDb;

    public function __construct()
    {
        $this->dataAlls = new ArrayCollection();
        $this->dataRegions = new ArrayCollection();
        $this->dataProvinces = new ArrayCollection();
        $this->dataDistricts = new ArrayCollection();
        $this->dataTypes = new ArrayCollection();
        $this->dataDateProvinces = new ArrayCollection();
        $this->dataDateRegions = new ArrayCollection();
        $this->dataDateDistricts = new ArrayCollection();
        $this->TranslationDb = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTranslatedName($locale)
    {
        return $this->translate($locale)->getName();
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * @return Collection|DataDateType[]
     */
    public function getDataAlls(): Collection
    {
        return $this->dataAlls;
    }

    public function addDataAll(DataDateType $dataAll): self
    {
        if (!$this->dataAlls->contains($dataAll)) {
            $this->dataAlls[] = $dataAll;
            $dataAll->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataAll(DataDateType $dataAll): self
    {
        if ($this->dataAlls->contains($dataAll)) {
            $this->dataAlls->removeElement($dataAll);
            // set the owning side to null (unless already changed)
            if ($dataAll->getTypeCase() === $this) {
                $dataAll->setTypeCase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataRegion[]
     */
    public function getDataRegions(): Collection
    {
        return $this->dataRegions;
    }

    public function addDataRegion(DataRegion $dataRegion): self
    {
        if (!$this->dataRegions->contains($dataRegion)) {
            $this->dataRegions[] = $dataRegion;
            $dataRegion->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataRegion(DataRegion $dataRegion): self
    {
        if ($this->dataRegions->contains($dataRegion)) {
            $this->dataRegions->removeElement($dataRegion);
            // set the owning side to null (unless already changed)
            if ($dataRegion->getTypeCase() === $this) {
                $dataRegion->setTypeCase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataProvince[]
     */
    public function getDataProvinces(): Collection
    {
        return $this->dataProvinces;
    }

    public function addDataProvince(DataProvince $dataProvince): self
    {
        if (!$this->dataProvinces->contains($dataProvince)) {
            $this->dataProvinces[] = $dataProvince;
            $dataProvince->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataProvince(DataProvince $dataProvince): self
    {
        if ($this->dataProvinces->contains($dataProvince)) {
            $this->dataProvinces->removeElement($dataProvince);
            // set the owning side to null (unless already changed)
            if ($dataProvince->getTypeCase() === $this) {
                $dataProvince->setTypeCase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDistrict[]
     */
    public function getDataDistricts(): Collection
    {
        return $this->dataDistricts;
    }

    public function addDataDistrict(DataDistrict $dataDistrict): self
    {
        if (!$this->dataDistricts->contains($dataDistrict)) {
            $this->dataDistricts[] = $dataDistrict;
            $dataDistrict->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataDistrict(DataDistrict $dataDistrict): self
    {
        if ($this->dataDistricts->contains($dataDistrict)) {
            $this->dataDistricts->removeElement($dataDistrict);
            // set the owning side to null (unless already changed)
            if ($dataDistrict->getTypeCase() === $this) {
                $dataDistrict->setTypeCase(null);
            }
        }

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|DataType[]
     */
    public function getDataTypes(): Collection
    {
        return $this->dataTypes;
    }

    public function addDataType(DataType $dataType): self
    {
        if (!$this->dataTypes->contains($dataType)) {
            $this->dataTypes[] = $dataType;
            $dataType->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataType(DataType $dataType): self
    {
        if ($this->dataTypes->contains($dataType)) {
            $this->dataTypes->removeElement($dataType);
            // set the owning side to null (unless already changed)
            if ($dataType->getTypeCase() === $this) {
                $dataType->setTypeCase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateProvince[]
     */
    public function getDataDateProvinces(): Collection
    {
        return $this->dataDateProvinces;
    }

    public function addDataDateProvince(DataDateProvince $dataDateProvince): self
    {
        if (!$this->dataDateProvinces->contains($dataDateProvince)) {
            $this->dataDateProvinces[] = $dataDateProvince;
            $dataDateProvince->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataDateProvince(DataDateProvince $dataDateProvince): self
    {
        if ($this->dataDateProvinces->contains($dataDateProvince)) {
            $this->dataDateProvinces->removeElement($dataDateProvince);
            // set the owning side to null (unless already changed)
            if ($dataDateProvince->getTypeCase() === $this) {
                $dataDateProvince->setTypeCase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateRegion[]
     */
    public function getDataDateRegions(): Collection
    {
        return $this->dataDateRegions;
    }

    public function addDataDateRegion(DataDateRegion $dataDateRegion): self
    {
        if (!$this->dataDateRegions->contains($dataDateRegion)) {
            $this->dataDateRegions[] = $dataDateRegion;
            $dataDateRegion->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataDateRegion(DataDateRegion $dataDateRegion): self
    {
        if ($this->dataDateRegions->contains($dataDateRegion)) {
            $this->dataDateRegions->removeElement($dataDateRegion);
            // set the owning side to null (unless already changed)
            if ($dataDateRegion->getTypeCase() === $this) {
                $dataDateRegion->setTypeCase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataDateDistrict[]
     */
    public function getDataDateDistricts(): Collection
    {
        return $this->dataDateDistricts;
    }

    public function addDataDateDistrict(DataDateDistrict $dataDateDistrict): self
    {
        if (!$this->dataDateDistricts->contains($dataDateDistrict)) {
            $this->dataDateDistricts[] = $dataDateDistrict;
            $dataDateDistrict->setTypeCase($this);
        }

        return $this;
    }

    public function removeDataDateDistrict(DataDateDistrict $dataDateDistrict): self
    {
        if ($this->dataDateDistricts->contains($dataDateDistrict)) {
            $this->dataDateDistricts->removeElement($dataDateDistrict);
            // set the owning side to null (unless already changed)
            if ($dataDateDistrict->getTypeCase() === $this) {
                $dataDateDistrict->setTypeCase(null);
            }
        }

        return $this;
    }

    public function getSettings(): ?string
    {
        return $this->settings;
    }

    public function setSettings(string $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @return Collection|Translation[]
     */
    public function getTranslationDb(): Collection
    {
        return $this->TranslationDb;
    }

    public function addTranslationDb(Translation $translationDb): self
    {
        if (!$this->TranslationDb->contains($translationDb)) {
            $this->TranslationDb[] = $translationDb;
        }

        return $this;
    }

    public function removeTranslationDb(Translation $translationDb): self
    {
        if ($this->TranslationDb->contains($translationDb)) {
            $this->TranslationDb->removeElement($translationDb);
        }

        return $this;
    }
}
