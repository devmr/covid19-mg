<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\ApiPlatform\DuplicateDataDateProvinces;
/**
 * @ApiResource(
 *     attributes={"order"={"date.date": "DESC"}},
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           },
 *          "put",
 *          "delete",
 *          "copy"={
 *              "method"="GET",
 *              "path"="/data_date_provinces/{id}/duplicate",
 *              "controller"=DuplicateDataDateProvinces::class,
 *          }
 *     },
 *     collectionOperations =
 *     {
 *          "post",
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                   "groups"={
 *                      "group:read"
 *                   }
 *               }
 *           }
 *     }
 * )
 * @ApiFilter(
 *     RangeFilter::class,
 *     properties={
 *        "number"
 *     }
 * )
 * @ApiFilter(
 *     DateFilter::class,
 *     properties={
 *         "date.date"
 *     }
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={
 *         "date.date",
 *         "number"
 *     },
 *     arguments={"orderParameterName"="order"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "typeCase": "exact",
 *          "province": "exact",
 *          "date.date": "exact"
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\DataDateProvinceRepository")
 */
class DataDateProvince
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Date", inversedBy="dataDateProvinces")
     * @Groups({"group:read"})
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"group:read"})
     */
    private $number;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"group:read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeCase", inversedBy="dataDateProvinces")
     * @Groups({"group:read"})
     */
    private $typeCase;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Province", inversedBy="dataDateProvinces")
     * @Groups({"group:read"})
     */
    private $province;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function setDate(?Date $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDisabledAt(): ?\DateTimeInterface
    {
        return $this->disabledAt;
    }

    public function setDisabledAt(?\DateTimeInterface $disabledAt): self
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    public function getTypeCase(): ?TypeCase
    {
        return $this->typeCase;
    }

    public function setTypeCase(?TypeCase $typeCase): self
    {
        $this->typeCase = $typeCase;

        return $this;
    }

    public function getProvince(): ?Province
    {
        return $this->province;
    }

    public function setProvince(?Province $province): self
    {
        $this->province = $province;

        return $this;
    }
    public function getName(): ?string
    {
        return $this->getNumber();
    }

    public function setName($name): ?self
    {
        return $this->setNumber($name);
    }
}
