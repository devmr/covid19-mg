<?php

namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class EntitiesTimestampablePersist
{

    public function prePersist(LifecycleEventArgs $args)
    {

        $entity = $args->getObject();


        if (!method_exists($entity,'setCreatedAt')) {
            return;
        }

        // setCreatedAt
        $entity->setCreatedAt(new \DateTime());

    }

    public function preUpdate(LifecycleEventArgs $args)
    {

        $update = true;

        $entity = $args->getObject();

        if (!method_exists($entity,'setUpdatedAt')) {
            return;
        }

        $class = get_class($entity);
        switch ($class) {
            case "App\Entity\DataType":
            case "App\Entity\DataProvince":
            case "App\Entity\DataRegion":
            case "App\Entity\DataDistrict":
            case "App\Entity\DataCommune":
                $update = false;
                break;
        }

        if (!$update) {
            return;
        }

        // setUpdatedAt
        $entity->setUpdatedAt(new \DateTime());

    }

}
