<?php

namespace App\Services;

use App\Entity\DataDateProvince;
use App\Entity\Province;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheData
{
    /**
     * @var EntityManager
     */
    private $em;

    private $cache;

    public function __construct(EntityManagerInterface $em, CacheInterface $cache)
    {
        $this->em = $em;
        $this->cache = $cache;
    }

    public function getSvgDailyDataProvince($id)
    {
        // all confirmed cases by Dates
        $confirmedCases = $this->em->getRepository(DataDateProvince::class)->findConfirmedCasesOrderByDate('ASC');

        $dataAll = $this->getData($confirmedCases);

        // all confirmed cases by Dates and provinceid
        $confirmedCases = $this->em->getRepository(DataDateProvince::class)->findConfirmedCasesByProvincesOrderByDate($id,'ASC');

        $data = $this->getData($confirmedCases, $dataAll[3]);

        list($firstDate,$lastDate,$total,$maxNumber,$confirmedCasesCount,$listStylesPercent,$echelleList, $listDate) = $data;

        $i = 0;

        // Width stroke
        $strokeWidth = 4;
        $firstX = 2.25;

        foreach ($listStylesPercent as $style) {
            $width = $i == 0 ? $firstX : $width+($strokeWidth-$firstX)+3;
            $i++;
        }

        $i = 0;
        $svgMl[] = '<div ><svg class="wrapper-stripe" width="' . ($width + $strokeWidth + 30). '">' . PHP_EOL;
        foreach ($listStylesPercent as $style) {
            $x = $i == 0 ? $firstX : $x+($strokeWidth-$firstX)+3;
            $svgMl[] = '<line data-date="' . $listDate[$i]->format('d/m') . '" class="linepercent ' . $style . '" x1="' . $x . '" y1="20" x2="' . $x . '" y2="100%" stroke-width="' . $strokeWidth . '"></line>' . PHP_EOL;
            $i++;
        }
        $svgMl[] = '<path
     class="hidden bubble" data-bubble
     d="m 2.6072295,0.16543192 -0.030308,20.01786608 3.51039,-4.41716 25.6933931,-0.0121 c 2.50742,-1.262046 2.222569,-4.010673 2.222569,-4.010673 l 0.04007,-11.58648641 z"
     inkscape:connector-curvature="0"
     sodipodi:nodetypes="ccccccc" />
  <text
    class="hidden text_inside_bubble" data-text_inside_bubble
     xml:space="preserve"
     x="17.354769"
     y="-33.938679">
     <tspan
       sodipodi:role="line"
       x="17.354769"
       y="-33.938679"
       class="textspan_inside_bubble" data-tooltip_textspan_stripe
       dy="43.799995"></tspan></text>' . PHP_EOL;
        $svgMl[] = '</svg>' . PHP_EOL;

        $svgMl[] = '</div>' . PHP_EOL;

        return $svgMl;
    }

    public function getCacheSvgDailyDataProvince($id)
    {
        $svgMl = $this->getSvgDailyDataProvince($id);

        // Store in cache
        return $this->cache->get('dataprovince_' . $id, function (ItemInterface $item) use ($svgMl) {
            return implode(' ',$svgMl);
        }, INF);
    }

    public function getDataDistrictsByProvince($id)
    {
        $province = $this->em->getRepository(Province::class)->find($id);
        return $province->getDistricts();
    }

    public function getCacheDataDistrictsByProvince($id)
    {
        $districts = $this->getDataDistrictsByProvince($id);

        return $this->cache->get('districtsprovince_' . $id, function (ItemInterface $item) use ($districts) {
            return $districts->getValues();
        }, INF);
    }

    private function getStylesPercentage($number)
    {
        switch (true) {
            case $number == 0:
                return 'percent-0';

            case $number>0 && $number<20:
                return 'percent-10';

            case $number>=20 && $number<40:
                return 'percent-20';

            case $number>=40 && $number<60:
                return 'percent-40';

            case $number>=60 && $number<90:
                return 'percent-60';

            default:
                return 'percent-100';
        }
    }

    private function getData($confirmedCases, $maxNumber=0)
    {
        $confirmedCasesCount = count($confirmedCases);

        $listDate = [];
        $listNumber = [];
        $i = 0;
        $total = 0;
        $firstDate = '';
        $lastDate = '';
        foreach ($confirmedCases as $row) {

            // number
            $number = $row->getNumber();
            $listNumber[] = $number;

            // Total
            $total += $number;

            // First Date
            if ($i == 0) {
                $firstDate = $row->getDate()->getDate();
            }

            // Last Date
            if ($i+1 == $confirmedCasesCount) {
                $lastDate = $row->getDate()->getDate();
            }

            $listDate[] = $row->getDate()->getDate();

            $i++;
        }

        // max number
        $maxNumber = $maxNumber>0 ?  $maxNumber : max($listNumber);


        // number + percent
        $listStylesPercent = [];
        foreach ($listNumber as $number) {
            $percent = $maxNumber>0 ? round(($number/$maxNumber)*100) : 0;
            $stylePercent = $this->getStylesPercentage($percent);

//            $listNumberTmp[] = [$number, $percent, $stylePercent];
            $listStylesPercent[] = $stylePercent;
        }

        // Echelle
        $echelleList[0] = 0;
        foreach ([20,40,60,90] as $percent) {
            $targetNumber = intval(floor(($percent*$maxNumber)/100));
            $echelleList[$percent] = $targetNumber;
        }
        $echelleList[100] = $maxNumber;


        return [ $firstDate,$lastDate,$total,$maxNumber,$confirmedCasesCount, $listStylesPercent, $echelleList, $listDate];
    }


}
