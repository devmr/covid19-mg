<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontendAboutpageController extends AbstractController
{
    /**
     * @Route({
     *  "fr": "/about",
     *  "mg": "/mg/about",
     *  "en": "/en/about"
     * }, name="frontend_about")
     */
    public function index()
    {
        return $this->render('frontend/aboutpage/index.html.twig', []);
    }
}
