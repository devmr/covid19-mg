<?php

namespace App\Controller\ApiPlatform;

use App\Entity\DataDateDistrict;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateDataDateDistricts extends AbstractController
{

    public function __invoke(DataDateDistrict $data): DataDateDistrict
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name );

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
