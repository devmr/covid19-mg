<?php

namespace App\Controller\ApiPlatform;

use App\Entity\Quartier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateQuartier extends AbstractController
{

    public function __invoke(Quartier $data): Quartier
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name . ' (copy)');

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
