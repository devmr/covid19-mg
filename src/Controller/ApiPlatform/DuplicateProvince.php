<?php

namespace App\Controller\ApiPlatform;

use App\Entity\Province;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateProvince extends AbstractController
{

    public function __invoke(Province $data): Province
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name . ' (copy)');

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
