<?php

namespace App\Controller\ApiPlatform;

use App\Entity\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateTranslation extends AbstractController
{

    public function __invoke(Translation $data): Translation
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name . ' (copy)');

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
