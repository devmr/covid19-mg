<?php

namespace App\Controller\ApiPlatform;

use App\Entity\DataDateProvince;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DuplicateDataDateProvinces extends AbstractController
{

    public function __invoke(DataDateProvince $data): DataDateProvince
    {
        $copy = clone $data;

        // Original Name
        $name = $data->getName();

        // Set new name
        $copy->setName( $name );

        $em = $this->getDoctrine()->getManager();
        $em->persist($copy);
        $em->flush();

        return $copy;
    }
}
