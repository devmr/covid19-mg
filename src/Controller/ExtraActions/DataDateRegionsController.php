<?php

namespace App\Controller\ExtraActions;

use App\Entity\DataDateRegion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DataDateRegionsController extends AbstractController
{
    /**
     * @Route("/api/data_date_regions/edit/{ids}", name="api_data_date_regions_getmultiple")
     */
    public function getMultiple($ids, SerializerInterface $serializer)
    {
        $rows = $this->getDoctrine()->getRepository(DataDateRegion::class)->findByIds($ids);
        $resultat = $serializer->serialize(
            ['hydra:member' => $rows],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

    /**
     * @Route("/api/data_date_regions/update-all", name="api_data_date_regions_updateall")
     */
    public function updateAll(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        if (empty($dataTab)) {
            return new JsonResponse([],200,[],true);
        }

        if (isset($dataTab['updatedAt'])) {
            $dataTab['updatedAt'] = (new \DateTime($dataTab['updatedAt']))->format('Y-m-d H:i:s');
        }

        $repo = $this->getDoctrine()->getRepository(DataDateRegion::class);
        $sql = $repo->updateAll($dataTab);

        $result = ['OK'];

        //cd
        $resultat = $serializer->serialize(
            ['hydra:member' => $result],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

}
