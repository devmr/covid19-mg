<?php

namespace App\Controller\ExtraActions;

use App\Entity\DataType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DataTypesController extends AbstractController
{
    /**
     * @Route("/api/data_types/edit/{ids}", name="api_data_types_getmultiple")
     */
    public function getMultiple($ids, SerializerInterface $serializer)
    {
        $rows = $this->getDoctrine()->getRepository(DataType::class)->findByIds($ids);
        $resultat = $serializer->serialize(
            ['hydra:member' => $rows],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }

    /**
     * @Route("/api/data_types/update-all", name="api_data_types_updateall")
     */
    public function updateAll(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        if (empty($dataTab)) {
            return new JsonResponse([],200,[],true);
        }

        if (isset($dataTab['updatedAt'])) {
            $dataTab['updatedAt'] = (new \DateTime($dataTab['updatedAt']))->format('Y-m-d H:i:s');
        }

        $repo = $this->getDoctrine()->getRepository(DataType::class);
        $sql = $repo->updateAll($dataTab);

        $result = ['OK'];

        //cd
        $resultat = $serializer->serialize(
            ['hydra:member' => $result],
            'json',
            [
                'groups'=>['group:read']
            ]
        );

        return new JsonResponse($resultat,200,[],true);
    }
}
