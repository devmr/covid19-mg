<?php

namespace App\Controller\ExtraActions;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LocaleController extends AbstractController
{
    /**
     * @Route("/api/locale", name="api_platform_locale")
     */
    public function locale()
    {
        $data = $this->getParameter('fallbacks_locale');

        return $this->json(['hydra:member' => $data]);
    }
}
