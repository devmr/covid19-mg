<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200401213335 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE commune (id INT AUTO_INCREMENT NOT NULL, province_id INT DEFAULT NULL, region_id INT DEFAULT NULL, district_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_E2E2D1EEE946114A (province_id), INDEX IDX_E2E2D1EE98260155 (region_id), INDEX IDX_E2E2D1EEB08FA272 (district_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_all (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, number INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_7F949068B897366B (date_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_commune (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, commune_id INT DEFAULT NULL, number INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_86E9871FB897366B (date_id), INDEX IDX_86E9871F131A4F72 (commune_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_district (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, district_id INT DEFAULT NULL, number INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_FB1F9D5BB897366B (date_id), INDEX IDX_FB1F9D5BB08FA272 (district_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_province (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, province_id INT DEFAULT NULL, number INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_80041DD7B897366B (date_id), INDEX IDX_80041DD7E946114A (province_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_region (id INT AUTO_INCREMENT NOT NULL, date_id INT DEFAULT NULL, region_id INT DEFAULT NULL, number INT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_6F9CC06FB897366B (date_id), INDEX IDX_6F9CC06F98260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE date (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE district (id INT AUTO_INCREMENT NOT NULL, province_id INT DEFAULT NULL, region_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_31C15487E946114A (province_id), INDEX IDX_31C1548798260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE province (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quartier (id INT AUTO_INCREMENT NOT NULL, province_id INT DEFAULT NULL, region_id INT DEFAULT NULL, district_id INT DEFAULT NULL, commune_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_FEE8962DE946114A (province_id), INDEX IDX_FEE8962D98260155 (region_id), INDEX IDX_FEE8962DB08FA272 (district_id), INDEX IDX_FEE8962D131A4F72 (commune_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, province_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, disabled_at DATETIME DEFAULT NULL, INDEX IDX_F62F176E946114A (province_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commune ADD CONSTRAINT FK_E2E2D1EEE946114A FOREIGN KEY (province_id) REFERENCES province (id)');
        $this->addSql('ALTER TABLE commune ADD CONSTRAINT FK_E2E2D1EE98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE commune ADD CONSTRAINT FK_E2E2D1EEB08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE data_all ADD CONSTRAINT FK_7F949068B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_commune ADD CONSTRAINT FK_86E9871FB897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_commune ADD CONSTRAINT FK_86E9871F131A4F72 FOREIGN KEY (commune_id) REFERENCES commune (id)');
        $this->addSql('ALTER TABLE data_district ADD CONSTRAINT FK_FB1F9D5BB897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_district ADD CONSTRAINT FK_FB1F9D5BB08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE data_province ADD CONSTRAINT FK_80041DD7B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_province ADD CONSTRAINT FK_80041DD7E946114A FOREIGN KEY (province_id) REFERENCES province (id)');
        $this->addSql('ALTER TABLE data_region ADD CONSTRAINT FK_6F9CC06FB897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('ALTER TABLE data_region ADD CONSTRAINT FK_6F9CC06F98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C15487E946114A FOREIGN KEY (province_id) REFERENCES province (id)');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C1548798260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE quartier ADD CONSTRAINT FK_FEE8962DE946114A FOREIGN KEY (province_id) REFERENCES province (id)');
        $this->addSql('ALTER TABLE quartier ADD CONSTRAINT FK_FEE8962D98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE quartier ADD CONSTRAINT FK_FEE8962DB08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE quartier ADD CONSTRAINT FK_FEE8962D131A4F72 FOREIGN KEY (commune_id) REFERENCES commune (id)');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176E946114A FOREIGN KEY (province_id) REFERENCES province (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_commune DROP FOREIGN KEY FK_86E9871F131A4F72');
        $this->addSql('ALTER TABLE quartier DROP FOREIGN KEY FK_FEE8962D131A4F72');
        $this->addSql('ALTER TABLE data_all DROP FOREIGN KEY FK_7F949068B897366B');
        $this->addSql('ALTER TABLE data_commune DROP FOREIGN KEY FK_86E9871FB897366B');
        $this->addSql('ALTER TABLE data_district DROP FOREIGN KEY FK_FB1F9D5BB897366B');
        $this->addSql('ALTER TABLE data_province DROP FOREIGN KEY FK_80041DD7B897366B');
        $this->addSql('ALTER TABLE data_region DROP FOREIGN KEY FK_6F9CC06FB897366B');
        $this->addSql('ALTER TABLE commune DROP FOREIGN KEY FK_E2E2D1EEB08FA272');
        $this->addSql('ALTER TABLE data_district DROP FOREIGN KEY FK_FB1F9D5BB08FA272');
        $this->addSql('ALTER TABLE quartier DROP FOREIGN KEY FK_FEE8962DB08FA272');
        $this->addSql('ALTER TABLE commune DROP FOREIGN KEY FK_E2E2D1EEE946114A');
        $this->addSql('ALTER TABLE data_province DROP FOREIGN KEY FK_80041DD7E946114A');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C15487E946114A');
        $this->addSql('ALTER TABLE quartier DROP FOREIGN KEY FK_FEE8962DE946114A');
        $this->addSql('ALTER TABLE region DROP FOREIGN KEY FK_F62F176E946114A');
        $this->addSql('ALTER TABLE commune DROP FOREIGN KEY FK_E2E2D1EE98260155');
        $this->addSql('ALTER TABLE data_region DROP FOREIGN KEY FK_6F9CC06F98260155');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C1548798260155');
        $this->addSql('ALTER TABLE quartier DROP FOREIGN KEY FK_FEE8962D98260155');
        $this->addSql('DROP TABLE commune');
        $this->addSql('DROP TABLE data_all');
        $this->addSql('DROP TABLE data_commune');
        $this->addSql('DROP TABLE data_district');
        $this->addSql('DROP TABLE data_province');
        $this->addSql('DROP TABLE data_region');
        $this->addSql('DROP TABLE date');
        $this->addSql('DROP TABLE district');
        $this->addSql('DROP TABLE province');
        $this->addSql('DROP TABLE quartier');
        $this->addSql('DROP TABLE region');
    }
}
