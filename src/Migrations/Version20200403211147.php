<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403211147 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_province DROP FOREIGN KEY FK_80041DD7B897366B');
        $this->addSql('DROP INDEX IDX_80041DD7B897366B ON data_province');
        $this->addSql('ALTER TABLE data_province DROP date_id');
        $this->addSql('ALTER TABLE data_region DROP FOREIGN KEY FK_6F9CC06FB897366B');
        $this->addSql('DROP INDEX IDX_6F9CC06FB897366B ON data_region');
        $this->addSql('ALTER TABLE data_region DROP date_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_province ADD date_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_province ADD CONSTRAINT FK_80041DD7B897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('CREATE INDEX IDX_80041DD7B897366B ON data_province (date_id)');
        $this->addSql('ALTER TABLE data_region ADD date_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data_region ADD CONSTRAINT FK_6F9CC06FB897366B FOREIGN KEY (date_id) REFERENCES date (id)');
        $this->addSql('CREATE INDEX IDX_6F9CC06FB897366B ON data_region (date_id)');
    }
}
