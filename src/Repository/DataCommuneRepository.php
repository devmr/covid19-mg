<?php

namespace App\Repository;

use App\Entity\DataCommune;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataCommune|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataCommune|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataCommune[]    findAll()
 * @method DataCommune[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataCommuneRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataCommune::class);
    }

    // /**
    //  * @return DataCommune[] Returns an array of DataCommune objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataCommune
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\DataCommune a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
