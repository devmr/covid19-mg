<?php

namespace App\Repository;

use App\Entity\DataDateDistrict;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataDateDistrict|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataDateDistrict|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataDateDistrict[]    findAll()
 * @method DataDateDistrict[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataDateDistrictRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataDateDistrict::class);
    }

    // /**
    //  * @return DataDateDistrict[] Returns an array of DataDateDistrict objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataDateDistrict
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', explode(',',$ids))
            ->getQuery()
            ->getResult()
        ;

        return $qb;
    }

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\DataDateDistrict a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
