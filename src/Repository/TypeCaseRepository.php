<?php

namespace App\Repository;

use App\Entity\TypeCase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeCase|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCase|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCase[]    findAll()
 * @method TypeCase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCaseRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];


    private $listFieldsOrdered = ['position'];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCase::class);
    }

    // /**
    //  * @return TypeCase[] Returns an array of TypeCase objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeCase
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllOrderedBy($field, $sort = 'ASC')
    {
         
        // Field is not in authorized field list
        if (!in_array($field, $this->listFieldsOrdered)) {
            return false;
        }

        if (!in_array($sort, ['ASC','DESC'])) {
            $sort = 'ASC';
        }

        return $this->createQueryBuilder('t')
            ->andWhere('t.disabledAt IS NULL')
            ->orderBy('t.' . $field, $sort)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findMaxPosition()
    {
        return $this->createQueryBuilder('t')
            ->select('MAX(t.position) position')
            ->orderBy('t.position', 'asc')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\TypeCase a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
