<?php

namespace App\Repository;

use App\Entity\DataDateProvince;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DataDateProvince|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataDateProvince|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataDateProvince[]    findAll()
 * @method DataDateProvince[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataDateProvinceRepository extends ServiceEntityRepository
{
    private $fieldsExclude = [
        'clone',
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataDateProvince::class);
    }

    // /**
    //  * @return DataDateProvince[] Returns an array of DataDateProvince objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataDateProvince
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findConfirmedCasesByProvincesOrderByDate($province, $sort)
    {

        if (!in_array($sort, ['ASC','DESC'])) {
            return false;
        }

        return $this->createQueryBuilder('d')
            ->join('d.date', 'e')
            ->where('d.typeCase = 1')
            ->andWhere('d.province = :province')
            ->setParameter('province', $province)
            ->orderBy('e.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findConfirmedCasesOrderByDate($sort)
    {

        if (!in_array($sort, ['ASC','DESC'])) {
            return false;
        }

        return $this->createQueryBuilder('d')
            ->join('d.date', 'e')
            ->where('d.typeCase = 1')
            ->orderBy('e.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByIds($ids)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id IN (:ids)')
            ->setParameter('ids', explode(',',$ids))
            ->getQuery()
            ->getResult()
        ;

        return $qb;
    }

    public function updateAll(array $fields) {
        if (empty($fields)) {
            return false;
        }

        $sqlFields = [];
        $paramFields = [];

        $sql = "UPDATE App\Entity\DataDateProvince a ";

        foreach ($fields as $key => $value) {

            if (!in_array($key, $this->fieldsExclude)) {
                $sqlFields[] = "a." . $key . " = :" . $key;
                $paramFields[$key] = $value;
            }
        }

        if (!empty($sqlFields)) {
            $sql .= " set ";
        }
        $sql .= implode(', ', $sqlFields);

        $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($paramFields)
            ->execute();
    }

}
