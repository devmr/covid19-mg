import ApexCharts from 'apexcharts'

export function renderLine(elem) {

    let backgroundColor = '#000000';
    let borderColor = '#000000';
    let typeChart = 'line';
    let data = [];
    let backgroundColorList = [];
    let strokeWidthList = [];
    let strokeDashList = [];
    let borderColorList = [];
    let labelsList = [];
    let chartData = [];

    // Labels - axis-x
    if (typeof elem.getAttribute('data-charts-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-label'));
        labelsList = JSON.parse(labels);
        chartData.labels = labelsList;
    }

    if (typeof elem.getAttribute('data-charts-backgroundcolor') !== 'undefined') {
        backgroundColorList = JSON.parse(elem.getAttribute('data-charts-backgroundcolor'));
    }
    if (typeof elem.getAttribute('data-charts-bordercolor') !== 'undefined') {
        borderColorList = JSON.parse(elem.getAttribute('data-charts-bordercolor'));
    }
    if (typeof elem.getAttribute('data-charts-stroke_width') !== 'undefined') {
        strokeWidthList = JSON.parse(elem.getAttribute('data-charts-stroke_width'));
    }
    if (typeof elem.getAttribute('data-charts-stroke_dash') !== 'undefined') {
        strokeDashList = JSON.parse(elem.getAttribute('data-charts-stroke_dash'));
    }

    if (typeof elem.getAttribute('data-charts-data') !== 'undefined') {
        data = JSON.parse(elem.getAttribute('data-charts-data'));
    }



    if (typeof elem.getAttribute('data-charts-dataset-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-dataset-label'));
        let datasetlabels = JSON.parse(labels);

        let i = 0;
        datasetlabels.forEach(dataset => {

            let datasets = {};
            // datasets.fill = false;
            // datasets.backgroundColor = backgroundColorList[i];
            // datasets.borderColor = borderColorList[i];
            datasets.name = decodeURI(dataset);
            datasets.data = data[i];
            chartData.push(datasets);

            i++;

        });
    }

    // alert(JSON.stringify(chartData));


    let config = {
        // Setting colors of series
        colors: backgroundColorList,

        series: chartData,
        chart: {
            height: 350,
            type: typeChart,
            zoom: {
                enabled: true
            },
            // Hide toolbar
            toolbar: {
                tools: {
                    download: false // Menu svg, png, csv
                }
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: strokeWidthList,
            curve: 'straight',
            dashArray: strokeDashList
        },
        title: {
            text: '',
                align: 'left'
        },
        legend: {
            tooltipHoverFormatter: function(val, opts) {
                return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
            }
        },
        markers: {
            size: 0,
                hover: {
                sizeOffset: 6
            }
        },
        xaxis: {
            categories: labelsList,
        },
        grid: {
            borderColor: '#f1f1f1',
        },
    };

    let chart = new ApexCharts(elem, config);
    chart.render();
}

export function renderPie(elem) {

    let typeChart = 'pie';
    let data = [];
    let backgroundColorList = [];
    let datasetlabels = [];

    // Labels - axis-x
    if (typeof elem.getAttribute('data-charts-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-label'));
        labels = JSON.parse(labels);
    }

    if (typeof elem.getAttribute('data-charts-backgroundcolor') !== 'undefined') {
        backgroundColorList = JSON.parse(elem.getAttribute('data-charts-backgroundcolor'));
    }

    if (typeof elem.getAttribute('data-charts-data') !== 'undefined') {
        data = JSON.parse(elem.getAttribute('data-charts-data'));
    }


    if (typeof elem.getAttribute('data-charts-dataset-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-dataset-label'));
        datasetlabels = JSON.parse(labels);
    }

    let config = {
        title: {
            text: '',
            align: 'left'
        },
        // Data
        series: data,
        chart: {
            width: 380,
            type: typeChart,
        },
        // Setting colors of series
        colors: backgroundColorList,

        // Setting fill colors of paths
        fill: {
            colors: backgroundColorList
        },

        // Labels
        labels: datasetlabels,
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }],
        legend: {
            position: 'bottom'
        }
    };

    let chart = new ApexCharts(elem, config);
    chart.render();
}

export function renderScatter(elem, chartType) {

    let data = [];
    let labelsList = [];
    let chartData = [];
    let backgroundColorList = [];

    // Labels - axis-x
    if (typeof elem.getAttribute('data-charts-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-label'));
        labelsList = JSON.parse(labels);
    }

    if (typeof elem.getAttribute('data-charts-backgroundcolor') !== 'undefined') {
        backgroundColorList = JSON.parse(elem.getAttribute('data-charts-backgroundcolor'));
    }

    if (typeof elem.getAttribute('data-charts-data') !== 'undefined') {
        data = JSON.parse(elem.getAttribute('data-charts-data'));
    }



    if (typeof elem.getAttribute('data-charts-dataset-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-dataset-label'));
        let datasetlabels = JSON.parse(labels);

        let i = 0;
        datasetlabels.forEach(dataset => {

            let datasets = {};
            datasets.data = [];
            datasets.name = decodeURI(dataset);

            let j = 0;
            let dataSeriesList = [];
            // Each date
            labelsList.forEach( (date) => {
                date = parseInt(date);
                let count = parseInt(data[i][j]);
                if (count>0) {
                    dataSeriesList.push([date, count]);
                }
                j++;
            });

            datasets.data = dataSeriesList;

            chartData.push(datasets);

            i++;

        });
    }

    // alert(JSON.stringify(chartData));


    let config = {
        // Setting colors of series
        colors: backgroundColorList,

        series: chartData,
        chart: {
            height: 350,
            type: chartType,
            zoom: {
                enabled: true,
                type: 'xy'
            },
            // Hide toolbar
            toolbar: {
                tools: {
                    download: false // Menu svg, png, csv
                }
            }
        },
        xaxis: {
            type: 'datetime',
        },
        yaxis: {
            tickAmount: 7
        }
    };

    let chart = new ApexCharts(elem, config);
    chart.render();
}


