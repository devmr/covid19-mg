import BadgerAccordion from 'badger-accordion';
import "badger-accordion/dist/badger-accordion.scss";

const accordions = document.querySelectorAll('.accordion');

Array.from(accordions).forEach((accordion) => {
    const ba = new BadgerAccordion(accordion);
});

