import snapsvg from 'snapsvg-cjs';

let querySelector = document.querySelector.bind(document);

document.addEventListener('DOMContentLoaded', () => {

    if (document.querySelectorAll('[data-svg]').length) {
        let chartSvg = document.querySelectorAll('[data-svg]');
        chartSvg.forEach((elem) => {
            let g = Snap(elem);
            if (typeof g.attr('data-svg') === 'undefined') {
                return false;
            }
            if (typeof g.attr('data-json') === 'undefined') {
                return false;
            }
            // console.log(g.attr('data-svg'));
            Snap.load(g.attr('data-svg'), (f) => {

                g.append(f);

                function mapPathClick(path, text) {

                    g.select(path).click(() => {
                        // const region = path.substring(1);

                    });
                };

                drawMap();

                function drawMap() {
                    let dataJson = decodeURIComponent(g.attr('data-json'));
                    dataJson = JSON.parse(dataJson);
                    if (typeof dataJson === 'undefined') {
                        return false;
                    }

                    dataJson.map((element, index) => {

                        // console.log(g.select(element.selector) === null);
                        if (g.select(element.selector) === null) {
                            // Stop script because of null
                            return false;
                        }

                        g.select(element.selector).removeClass('active');
                        if (typeof element.active !== 'undefined' && element.active === 'true') {
                            g.select(element.selector).addClass('active');
                        }

                        // Show tooltip on hover
                        if (typeof element.tooltip !== 'undefined' && element.tooltip !== '') {
                            g.select(element.selector).mouseover(() => {
                                let tooltip = Snap.parse('<title>' + (typeof element.tooltip !== 'undefined' ? element.tooltip : '') + '</title>');
                                g.select(element.selector).append(tooltip);
                            });
                        }

                        // onclick path -> go to url item
                        if (typeof element.url !== 'undefined' && element.url !== '') {
                            g.select(element.selector).click(() => {
                                // go to url
                                window.location.href = element.url;
                            });
                        }
                    });

                }

                if (typeof g.attr('data-province') !== 'undefined') {
                    /**
                     *
                     * @returns {boolean}
                     */
                    function drawProvince() {
                        let dataJson = decodeURIComponent(g.attr('data-province'));
                        dataJson = JSON.parse(dataJson);
                        if (typeof dataJson === 'undefined') {
                            return false;
                        }
                        // console.log(dataJson.id1);

                        for (let obj in dataJson) {

                            let ellipseElement = '#circle_' + obj;
                            let textInsideEllipse = '#text_' + obj;
                            let textspanInsideEllipse = '#textspan_' + obj;
                            if (g.select(ellipseElement) === null && g.select(textInsideEllipse) === null) {
                                // Stop script because of null
                                return false;
                            }

                            let display = dataJson[obj].display;
                            let data = dataJson[obj].data;

                            // No Data
                            if (data === 0 || display === '') {
                                return;
                            }


                            // Position textes x*y
                            let positionTextX = parseInt(g.select(textspanInsideEllipse).attr('x'));
                            let positionTextY = parseInt(g.select(textspanInsideEllipse).attr('y'));

                            // Rayon ellipse x*y
                            let rayonEllipseX = parseInt(g.select(ellipseElement).attr('rx'));
                            let rayonEllipseY = parseInt(g.select(ellipseElement).attr('ry'));


                            // Show Ellipse
                            setTimeout(() => {



                                g.select(ellipseElement).removeClass('hidden');
                                // Add animation
                                g.select(ellipseElement).addClass('animated fadeInDown');

                                // Show text Ellipse
                                g.select(textInsideEllipse).removeClass('hidden');

                                // Add animation
                                g.select(textInsideEllipse).addClass('animated fadeInDown');

                                // Fill data
                                g.select(textspanInsideEllipse).node.innerHTML = display;

                            }, 500);


                            let classTxt = 'svg-text-21';
                            switch (true) {
                                // 10 - 99
                                case data >= 10 && data <= 99:
                                    classTxt = 'svg-text-18';
                                    positionTextX -= 4;
                                    break;

                                // 100 - 999
                                case data >= 100 && data <= 999:
                                    classTxt = 'svg-text-13';
                                    positionTextX -= 6;
                                    positionTextY -= 2;
                                    break;

                                // 1000 - 9999
                                case data >= 1000 && data <= 9999:
                                    classTxt = 'svg-text-9';
                                    positionTextY -= 4;
                                    positionTextX -= 5;
                                    break;

                                // 10000 - 99999
                                case data >= 10000 && data <= 99999:
                                    classTxt = 'svg-text-8';
                                    positionTextX -= 5;
                                    positionTextY -= 4;
                                    rayonEllipseX = 20;
                                    rayonEllipseY = 20;
                                    break;

                                // 100000 - 999999
                                case data >= 100000:
                                    classTxt = 'svg-text-13';
                                    positionTextX -= 15;
                                    positionTextY -= 2;
                                    rayonEllipseX = 30;
                                    rayonEllipseY = 30;
                                    break;
                            }

                            // Class text
                            g.select(textspanInsideEllipse).attr('class', classTxt);

                            // Position textes x*y
                            g.select(textspanInsideEllipse).attr('x', positionTextX);
                            g.select(textInsideEllipse).attr('x', positionTextX);
                            g.select(textspanInsideEllipse).attr('y', positionTextY);
                            g.select(textInsideEllipse).attr('y', positionTextY);

                            // Rayon ellipse x*y
                            g.select(ellipseElement).attr('rx', rayonEllipseX);
                            g.select(ellipseElement).attr('ry', rayonEllipseY);

                        }

                    }

                    drawProvince();
                }



            });
        });
    }
});
