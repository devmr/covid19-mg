import ApexCharts from 'apexcharts'

import * as chart from "../apexcharts/utils";

let chartsDom = Array.prototype.slice.call(document.querySelectorAll('[data-chart]'), 0);
if (chartsDom.length > 0) {

    chartsDom.forEach( elem => {

        let type = elem.getAttribute('data-charts-type');

        switch (type) {

            case 'pie':
                chart.renderPie(elem);
                break;

            case 'line':
            case 'scatter':
                chart.renderScatter(elem, type);
                break;
        }
    })
}
