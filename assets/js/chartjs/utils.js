
export function renderBar ()  {
        let backgroundColor = null;
        let borderColor = null;
        let typeChart = 'bar';
        let chartjsData = {};

        if (typeof elem.getAttribute('data-charts-label') !== 'undefined') {
            let labels = decodeURIComponent(elem.getAttribute('data-charts-label'));
            labels = JSON.parse(labels);
            chartjsData.labels = labels;
        }
        chartjsData.datasets = [];
        chartjsData.datasets[0] = {};
        if (typeof elem.getAttribute('data-charts-dataset-label') !== 'undefined') {
            let datasetlabel = elem.getAttribute('data-charts-dataset-label');
            chartjsData.datasets[0].label = datasetlabel;
        }

        chartjsData.datasets[0].fill = false;
        if (typeof elem.getAttribute('data-charts-data') !== 'undefined') {
            let data = JSON.parse(elem.getAttribute('data-charts-data'));
            chartjsData.datasets[0].data = data;
        }
        if (typeof elem.getAttribute('data-charts-backgroundcolor') !== 'undefined') {
            backgroundColor = JSON.parse(elem.getAttribute('data-charts-backgroundcolor'));
            chartjsData.datasets[0].backgroundColor = backgroundColor;
        }
        if (typeof elem.getAttribute('data-charts-bordercolor') !== 'undefined') {
            borderColor = JSON.parse(elem.getAttribute('data-charts-bordercolor'));
            chartjsData.datasets[0].borderColor = borderColor;
        }


        new Chart(
            elem,
            {
                "type": typeChart,
                "data": chartjsData,
                "options":
                    {
                        "scales":
                            {
                                "yAxes":
                                    [
                                        {
                                            "ticks":
                                                {
                                                    "beginAtZero":true
                                                }
                                        }
                                    ]
                            }
                    }
            });
}


export function renderLine(elem) {

    let backgroundColor = '#000000';
    let borderColor = '#000000';
    let typeChart = 'line';
    let data = [];
    let backgroundColorList = [];
    let borderColorList = [];
    let chartjsData = {};

    // Labels - axis-x
    if (typeof elem.getAttribute('data-charts-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-label'));
        labels = JSON.parse(labels);
        chartjsData.labels = labels;
    }

    if (typeof elem.getAttribute('data-charts-backgroundcolor') !== 'undefined') {
        backgroundColorList = JSON.parse(elem.getAttribute('data-charts-backgroundcolor'));
    }
    if (typeof elem.getAttribute('data-charts-bordercolor') !== 'undefined') {
        borderColorList = JSON.parse(elem.getAttribute('data-charts-bordercolor'));
    }

    if (typeof elem.getAttribute('data-charts-data') !== 'undefined') {
        data = JSON.parse(elem.getAttribute('data-charts-data'));
    }

    chartjsData.datasets = [];


    if (typeof elem.getAttribute('data-charts-dataset-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-dataset-label'));
        let datasetlabels = JSON.parse(labels);

        let i = 0;
        datasetlabels.forEach(dataset => {

            let datasets = {};
            datasets.fill = false;
            datasets.backgroundColor = backgroundColorList[i];
            datasets.borderColor = borderColorList[i];
            datasets.label = decodeURI(dataset);
            datasets.data = data[i];
            chartjsData.datasets.push(datasets);

            i++;

        });
    }


    let config = {
        "type": typeChart,
        "data": chartjsData,
        "options":
            {
                "scales":
                    {
                        "yAxes":
                            [
                                {
                                    "ticks":
                                        {
                                            "beginAtZero": true
                                        }
                                }
                            ]
                    }
            }
    };

    new Chart(elem, config);
}
export function renderPie(elem) {

    let typeChart = 'pie';
    let data = [];
    let backgroundColorList = [];
    let chartjsData = {};

    let datasets = {};

    // Labels - axis-x
    if (typeof elem.getAttribute('data-charts-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-label'));
        labels = JSON.parse(labels);
        chartjsData.labels = labels;
    }

    if (typeof elem.getAttribute('data-charts-backgroundcolor') !== 'undefined') {
        backgroundColorList = JSON.parse(elem.getAttribute('data-charts-backgroundcolor'));
        datasets.backgroundColor = backgroundColorList;
    }

    if (typeof elem.getAttribute('data-charts-data') !== 'undefined') {
        data = JSON.parse(elem.getAttribute('data-charts-data'));
        datasets.data = data;
    }


    if (typeof elem.getAttribute('data-charts-dataset-label') !== 'undefined') {
        let labels = decodeURIComponent(elem.getAttribute('data-charts-dataset-label'));
        let datasetlabels = JSON.parse(labels);
        chartjsData.labels = datasetlabels;
    }

    chartjsData.datasets = [];
    chartjsData.datasets.push(datasets);


    let config = {
        "type": typeChart,
        "data": chartjsData,
        "options": {
            responsive: true
        }
    };

    new Chart(elem, config);
}


